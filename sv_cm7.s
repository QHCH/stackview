    AREA    |.text|, CODE, READONLY

    IMPORT  sv
    EXPORT  sv_register_asm     ; see @sv_register for it's working process
sv_register_asm
check_param
    CMP     R0, #0
    BEQ     error
    AND     R2, R0, #3
    CMP     R2, #0
    BNE     error
    CMP     R1, #0
    BEQ     error
set_param
    LDR     R2, =sv
    STR     R0, [R2]
    MOV     R3, #4
    MUL     R3, R1, R3
    STR     R3, [R2, #4]
    MOV     R2, #4
    MUL     R1, R1, R2
    SUB     R2, R0, R1
    MOV     R3, #0xA5
padding_stack
    STRB    R3, [R2]
    ADD     R2, R2, #1
    CMP     R2, R0
    BNE     padding_stack
    MOV     R0, #0
    B       ok
error
    MOV     R0, #1
ok
    BX      LR
            ALIGN

    EXPORT  sv_checkout_asm     ; see @sv_checkout for it's working process
sv_checkout_asm
    CMP     R0, #0
    BEQ     error2
    CMP     R1, #0
    BEQ     error2
    LDR     R4, =sv
    LDR     R2, [R4]            ; R2 = sv.st
    LDR     R3, [R4, #4]        ; R3 = sv.total
    CMP     SP, R2
    BGE     surplus_is_minus    ; if SP >= sv.st
    SUB     R4, R2, R3          ; R4 = sv.st - sv.total
    CMP     SP, R4
    BLT     surplus_is_minus    ; if SP < sv.st - sv.total
    MOV     R4, SP
    SUB     R4, R2, R4
    SUB     R5, R3, R4
    STR     R5, [R1]            ; *sur = sv.total - (sv.st - get_SP());
    B       get_surplus_done
surplus_is_minus
    MOV     R4, #-1
    STR     R4, [R1]            ; *sur = -1
get_surplus_done
start_counting
    MOV     R1, #0              ; i = 0
    SUB     R4, R2, R3          ; p = (uint8_t *)(sv.st) - sv.total, R4 = p
    CMP     R4, R2
    BEQ     counting_done
counting
    LDRB    R3, [R4]            ; R3 = *p
    CMP     R3, #0xA5
    BNE     counting_done
    ADD     R1, R1, #1
    ADD     R4, R4, #1
    CMP     R4, R2
    BNE     counting
counting_done
    STR     R1, [R0]
    MOV     R0, #0
    BX      LR
error2
    MOV     R0, #1
    BX      LR
            ALIGN

    END
