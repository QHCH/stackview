#include "sv.h"

#define CONFIG_STACK_SIZE       (0x400)     /* stack size(in bytes) */
#define MAGICPAD                (0xA5)

struct {
    uint32_t *st;           // stack top address
    uint32_t total;         // stack total size(in bytes)
} sv;                       // stack view

/**
 * register a stack instance
 * @param st: the stack address
 * @param len: the length(in words) of @st
 * @return: 0 --- success
 *          others --- failed
 * @note: this program assumes the stack growth direction is down. so the @st is
 *        the end address of the stack. eg: if the stack memory range is
 *        [0x800, 0x900), the @st would be 0x900
 * int32_t sv_register(uint32_t *st, uint32_t len) {
 *     uint8_t *p;
 *
 *     if (!len || !st || ((uint32_t)st & 0x3)) {
 *         return 1;
 *     }
 *
 *     for (p = (uint8_t *)(st - len); p != (uint8_t *)st; p++) {
 *         *p = MAGICPAD;
 *     }
 *     sv.st = st;
 *     sv.total = len * 4;
 *     return 0;
 * }
 */

/**
 * checkout the stack
 * @param min: the historical minimal surplus stack(unit: bytes)
 * @param sur: current surplus stack(unit: bytes). if the stack was overflowed, it would be -1
 * @note: this is a pseudo code
 * @return: 0 --- success
 *          others --- failure
 * int32_t sv_checkout(int32_t *min, int32_t *sur) {
 *     uint32_t i;
 *     uint8_t *p;
 *
 *     if (!min || !sur) {
 *         return 1;
 *     }
 *
 *     if (get_SP() >= sv.st || get_SP() < sv.st - sv.total) {
 *         *sur = -1;
 *     } else {
 *         *sur = sv.total - (sv.st - get_SP());
 *     }
 *
 *     for (i = 0, p = (uint8_t *)(sv.st) - sv.total; p != (uint8_t *)(sv.st); p++) {
 *         if (*p == MAGICPAD) {
 *             i++;
 *         } else {
 *             break;
 *         }
 *     }
 *     *min = i;
 *     return 0;
 * }
 */

int $Sub$$main(void)
{
    extern int32_t sv_register_asm(uint32_t *st, uint32_t len);
    extern void $Super$$main(void);
    extern uint32_t __initial_sp;

    sv_register_asm(&__initial_sp, CONFIG_STACK_SIZE / 4);
    $Super$$main();
    return 0;
}
