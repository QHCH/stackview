This is a stack viewer for C language. It can checkout the stack surplus.

note:
    * this program can only be compiled by MDK
    * this program is only adapted for those CPUs:
        * stack growth direction is descend
        * 32bit(which means the stack address is 4 bytes alignment)
    * the stack surplus report may not be very accuracy
    * this program can only checkout main-stack(ARM Cotex-M call it MSP), not thread-stack(PSP)

demo usage:
    environment: MDK5, STM32F746
    what it does: checkout the main-stack surplus size

    1. add sv.c, sv_cm[0|7].s to your project, and sv.h to the include path
    2. assign @CONFIG_STACK_SIZE in sv.c base on your project
    3. add this code in any function using the main-stack:
        #include "sv.h"
        
        int32_t min;
        int32_t sur;
        
        if (!sv_checkout_asm(&min, &sur)) {
            printf("stack historical minimal surplus=%dB, current surplus=%dB\r\n", min, sur);
        }
    4. end
