#ifndef _SV_H_
#define _SV_H_

#include <stdint.h>

int32_t sv_checkout_asm(int32_t *min, int32_t *sur);

#endif
